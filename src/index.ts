import express from 'express'

// Create an Express app
const app = express()

// Define routes
app.get('/', (req, res) => {
  res.send('Hello, World!')
})

// Start the server
const port = 3000
app.listen(port, () => {
  console.log(`Server running on port http://localhost:${port}`)
})
